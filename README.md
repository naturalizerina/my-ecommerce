# E-commerce
Learn how to build a simple e-commerce application using client server architecture.

## Learn About
- Android Studio
- Volley
- JSON
- CRUD

## Main Page
![](./main.png)

## Detail Page
![](./detail.png)

## Cart Page
![](./cart.png)
