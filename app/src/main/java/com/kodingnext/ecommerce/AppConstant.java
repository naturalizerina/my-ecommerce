package com.kodingnext.ecommerce;

//the function of this class is for storing server address
public class AppConstant {

    //AVD -> IP 10.0.2.2
    //genymotion -> IP 10.0.3.2

//    sales kmy = "https://ickodingnext.com/andev/saleskmy/api/";
//    sales pik = "https://ickodingnext.com/andev/salespik/api/";
//    student = "https://ickodingnext.com/andev/api/";
    public static String baseURL = "https://ickodingnext.com/andev/api/";

    public static String getProduct = baseURL + "getproduct.php";
    public static String getCart = baseURL + "getcart.php";
    public static String addToCart = baseURL + "addtocart.php";
    public static String updateCart = baseURL + "updatecart.php";
    public static String removeCart = baseURL + "deletecart.php";

}