package com.kodingnext.ecommerce;

import android.app.Dialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;

//make custom dialog
public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
        //when dialog appears, the background is going to dark
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //set background to transparent
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //hide the title section
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //user cannot hiding the dialog, directly except from code, like -> dialog.dismiss()
        setCancelable(false);
        //setting layout for dialog
        setContentView(R.layout.layout_custom_dialog);
    }

}
