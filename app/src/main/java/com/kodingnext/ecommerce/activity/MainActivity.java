package com.kodingnext.ecommerce.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kodingnext.ecommerce.AppConstant;
import com.kodingnext.ecommerce.CustomDialog;
import com.kodingnext.ecommerce.R;
import com.kodingnext.ecommerce.VolleyRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import naturalizer.separator.Separator;

public class MainActivity extends AppCompatActivity {

    //initialize component
    RecyclerView rv_product;
    AdapterProduct adapterProduct;
    ArrayList<ModelProduct> dataProduct;
    CustomDialog customDialog;
    TextView tv_item_cart;
    FrameLayout btn_badge;

    //initialize default item count value for cart
    int mCartItemCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize component
        rv_product = findViewById(R.id.product_rv);
        GridLayoutManager glm = new GridLayoutManager(MainActivity.this, 2);
        rv_product.setLayoutManager(glm);
        rv_product.addItemDecoration(new RecyclerViewMargin(20, 2));
        customDialog = new CustomDialog(MainActivity.this);
        tv_item_cart = findViewById(R.id.cart_badge);
        btn_badge = findViewById(R.id.btn_badge);

        //make action when click cart icon
        btn_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this, CartActivity.class),0);
            }
        });

        //get data from server to fetching product data item
        getDataProduct();
    }

    private void getDataProduct() {

        //showing the dialog
        customDialog.show();
        //setup request method(GET), URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConstant.getProduct, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //hide the dialog
                customDialog.dismiss();

                //renew the arraylist data
                dataProduct = new ArrayList<>();

                //analyze JSON data structure from variable response -> .. onResponse(String response) {
                //try catch is must, if you parsing the json
                try {
                    //get json object of response??
                    JSONObject parent = new JSONObject(response);
                    //get json array from parent json object
                    JSONArray jsonArray = parent.getJSONArray("data");

                    //loop the json array to fetch each row of all data list
                    for (int i = 0; i < jsonArray.length(); i++) {

                        //get json object for i value position
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        //get the atribut depend of their name
                        int id = jsonObject.getInt("id_product");
                        String name = jsonObject.getString("name");
                        String price = jsonObject.getString("price");
                        String description = jsonObject.getString("description");
                        String image = jsonObject.getString("image");

                        //create new row of data that representated in each model created
                        ModelProduct modelProduct = new ModelProduct(
                                id,
                                name,
                                price,
                                description,
                                image);

                        //add to array list data
                        dataProduct.add(modelProduct);
                    }

                    //============================ Cart Badge Start ==============================//
                    //get sum of cart item
                    mCartItemCount = parent.getInt("count_cart");

                    //check for hide and show the red badge
                    if (mCartItemCount == 0) {
                        //if item cart empty, than hide the red badge notification
                        if (tv_item_cart.getVisibility() != View.GONE) {
                            tv_item_cart.setVisibility(View.GONE);
                        }
                    } else {

                        //function to handle maximum item count displayed
                        tv_item_cart.setText(String.valueOf(Math.min(mCartItemCount, 99)));

                        //if cart not zero, show the red badge notification
                        if (tv_item_cart.getVisibility() != View.VISIBLE) {
                            tv_item_cart.setVisibility(View.VISIBLE);
                        }
                    }
                    //============================= Cart Badge End ===============================//

                    //initialize and fill adapter product with data that have been returned from json
                    adapterProduct = new AdapterProduct(dataProduct);

                    //connect recyclerview to adapter product
                    rv_product.setAdapter(adapterProduct);

                } catch (JSONException e) {
                    //when request parsing error, hide the dialog
                    customDialog.dismiss();
                    //print error message
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //when request to server error, hide the dialog
                customDialog.dismiss();
            }
        });

        //applying to start the request data to server
        VolleyRequest.getInstance().addToRequestQueue(stringRequest);
    }

    //the function of this class is for customing and creating padding or margin for the recyclerview
    public class RecyclerViewMargin extends RecyclerView.ItemDecoration {

        //column is for accomodate the number of columns
        int columns;
        //margin is for accomodate the size of margin
        int margin;

        public RecyclerViewMargin(int margin, int columns) {
            //passing the margin from margin that defined outside to inside
            this.margin = margin;
            //passing the columns from column that defined outside to inside
            this.columns = columns;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            //get the position to determine the condition of decoration
            int position = parent.getChildLayoutPosition(view);

            // == Right and Left margin==
            outRect.right = margin;
            //if the position is even then add left margin
            //(when position in 0, 2, 4, 6, 8) give left margin, for example 0 / 2 = a  -> 0 = 2 x a, then a is 0 to get the 0 result in the left (0 = ..)
            if (position % columns == 0) outRect.left = margin;
            // == Right and Left margin==

            // == Top and Bottom margin==
            //if the position is in top or in 0 or 1 position then add the top margins
            if (position < columns) outRect.top = margin;
            outRect.bottom = margin;
            // == Top and Bottom margin==

        }
    }

    //this class is for connecting the data to each item in the layout, and you must create the view holder first
    class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ProductViewHolder> {

        //create arraylist for accomodate the arraylist data
        ArrayList<ModelProduct> dataProduct = new ArrayList<ModelProduct>();

        //create constructor for passing
        public AdapterProduct(ArrayList<ModelProduct> dataProduct) {
            //passing the data from dataProduct outside to dataProduct inside
            this.dataProduct = dataProduct;

            //refreshing list data
            notifyDataSetChanged();
        }

        //for inflate the layout for each of item
        @NonNull
        @Override
        public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //create view for item
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false);
            return new ProductViewHolder(itemView);
        }

        //fetching the data
        @Override
        public void onBindViewHolder(ProductViewHolder productViewHolder, int i) {
            productViewHolder.tv_price.setText("Rp. " + Separator.getInstance().doSeparate(dataProduct.get(i).price, Locale.GERMANY));
            productViewHolder.tv_name.setText(dataProduct.get(i).name);
            Picasso.get().load(dataProduct.get(i).image).into(productViewHolder.iv_product);
        }

        //set size of list that appears in UI from size of data size in arraylist data product
        @Override
        public int getItemCount() {
            return dataProduct.size();
        }

        //to initialize and create click action
        //don't forget to add -> implements View.OnClickListener to add click function
        public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            //initialize component text and image
            TextView tv_price, tv_name;
            ImageView iv_product;

            public ProductViewHolder(View itemView) {
                super(itemView);
                //initialize click function
                itemView.setOnClickListener(this);

                //initialize component text and image
                tv_price = itemView.findViewById(R.id.price_tv);
                tv_name = itemView.findViewById(R.id.name_tv);
                iv_product = itemView.findViewById(R.id.product_iv);
            }


            //make click function action here
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DetailActivity.class);
                //passing data to destination page from current position
                i.putExtra("data", dataProduct.get(getAdapterPosition()));
                //startactivityforresult used to make an action when this page appears again
                startActivityForResult(i, 0);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //the action when this page appears again is refreshing product data
        getDataProduct();
    }
}
