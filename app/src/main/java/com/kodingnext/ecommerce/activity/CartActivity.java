package com.kodingnext.ecommerce.activity;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kodingnext.ecommerce.AppConstant;
import com.kodingnext.ecommerce.CustomDialog;
import com.kodingnext.ecommerce.R;
import com.kodingnext.ecommerce.VolleyRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import naturalizer.separator.Separator;


public class CartActivity extends AppCompatActivity {

    //initialize component
    RecyclerView rv_cart;
    AdapterCart adapterCart;
    ArrayList<ModelCart> dataCart;
    Button btn_update;
    CustomDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        //initialize component
        rv_cart = findViewById(R.id.cart_rv);
        LinearLayoutManager llm = new LinearLayoutManager(CartActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_cart.setLayoutManager(llm);
        rv_cart.addItemDecoration(new RecyclerViewMargin(20));
        btn_update = findViewById(R.id.btn_update);
        customDialog = new CustomDialog(CartActivity.this);

        //get data cart
        getCart();

        //do action to update data cart
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if data cart is not empty, then update data cart
                if (dataCart.size() > 0) updateCart();
            }
        });

    }

    //function data cart
    private void getCart() {

        //showing dialog
        customDialog.show();
        //setup request method(GET), URL
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConstant.getCart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //hide the dialog
                customDialog.dismiss();

                //renew the arraylist data
                dataCart = new ArrayList<>();

                //analyze JSON data structure from variable response -> .. onResponse(String response) {
                //try catch is must, if you parsing the json
                try {
                    //get json object of response
                    JSONObject parent = new JSONObject(response);
                    //get json array from parent json object
                    JSONArray jsonArray = parent.getJSONArray("data");

                    //loop the json array to fetch each row of all data list
                    for (int i = 0; i < jsonArray.length(); i++) {

                        //get json object for i value position
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        //get the atribut depend of their name
                        int id_cart = jsonObject.getInt("id_cart");
                        int id_product = jsonObject.getInt("id_product");
                        int qty = jsonObject.getInt("qty");
                        int total_price = jsonObject.getInt("total_price");
                        String image = jsonObject.getString("image");
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        int item_price = jsonObject.getInt("item_price");

                        //create new row of data that representated in each model created
                        ModelCart modelCart = new ModelCart(
                                id_cart,
                                id_product,
                                qty,
                                total_price,
                                image,
                                name,
                                description,
                                item_price
                        );

                        //add to array list data
                        dataCart.add(modelCart);
                    }

                    //initialize and fill adapter cart with daa that have been returned from json
                    adapterCart = new AdapterCart(dataCart);

                    //connect recyclerview to adapter cart
                    rv_cart.setAdapter(adapterCart);

                } catch (JSONException e) {
                    //when request parsing error, hide the dialog
                    customDialog.dismiss();
                    //print error message
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //when request to server error, hide the dialog
                customDialog.dismiss();
            }
        });

        //applying to start the request data to server
        VolleyRequest.getInstance().addToRequestQueue(stringRequest);
    }

    //the function of this class is for customing and creating padding or margin for the recyclerview
    public class RecyclerViewMargin extends RecyclerView.ItemDecoration {

        //for accomodate space
        private final int space;

        public RecyclerViewMargin(int space) {
            //passing the space from space that defined outside to inside
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            //get position to determine the condition of decoration
            int position = parent.getChildLayoutPosition(view);
            //if the position is in top of list, then ad the top space
            if (position == 0) outRect.top = space;
            //adding left space
            outRect.left = space;
            //adding right space
            outRect.right = space;
            //adding bottom space
            outRect.bottom = space;
        }
    }

    class ModelCart {

        int id_cart;
        int id_product;
        int qty;
        int total_price;

        String image;
        String name;
        String description;
        int item_price;

        public ModelCart(int id_cart, int id_product, int qty, int total_price, String image, String name, String description, int item_price) {
            this.id_cart = id_cart;
            this.id_product = id_product;
            this.name = name;
            this.total_price = total_price;
            this.item_price = item_price;
            this.description = description;
            this.qty = qty;
            this.image = image;

        }

    }

    //this class is for connecting the data to each item in the layout, and you must create the view holder first
    class AdapterCart extends RecyclerView.Adapter<AdapterCart.CartViewHolder> {

        //create arraylist for accomodate the arraylist data
        ArrayList<ModelCart> dataCart = new ArrayList<>();

        //create constructor for passing
        public AdapterCart(ArrayList<ModelCart> dataCart) {
            //passing the data from dataProduct outside to dataProduct inside
            this.dataCart = dataCart;

            //refreshing list data
            notifyDataSetChanged();
        }

        //for inflate the layout for each of item
        @NonNull
        @Override
        public CartViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            //create view for item
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cart, viewGroup, false);
            return new CartViewHolder(itemView);
        }

        //fetching the data
        @Override
        public void onBindViewHolder(@NonNull CartViewHolder cartViewHolder, int i) {
            cartViewHolder.tv_name.setText(dataCart.get(i).name);
            cartViewHolder.tv_desc.setText(dataCart.get(i).description);

            Picasso.get().load(dataCart.get(i).image).into(cartViewHolder.iv_product);
            cartViewHolder.tv_qty.setText("" + dataCart.get(i).qty);
            cartViewHolder.tv_price.setText("Rp. " + Separator.getInstance().doSeparate("" + dataCart.get(i).total_price, Locale.GERMANY));
        }

        //set size of list that appears in UI from size of data size in arraylist data cart
        @Override
        public int getItemCount() {
            return dataCart.size();
        }

        //the function is to bring the current list data of cart to update cart parameter
        public ArrayList<ModelCart> getData() {
            return dataCart;
        }

        //to initialize and create click action
        class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            //initialize component text and button
            TextView tv_qty, tv_desc, tv_price, tv_name;
            ImageView iv_product;
            Button btn_increase, btn_decrease;

            public CartViewHolder(@NonNull View itemView) {
                super(itemView);

                //initialize click function
                itemView.setOnClickListener(this);

                //initialize component text and button
                tv_name = itemView.findViewById(R.id.name_tv);
                tv_qty = itemView.findViewById(R.id.qty_tv);
                tv_desc = itemView.findViewById(R.id.desc_tv);
                iv_product = itemView.findViewById(R.id.product_iv);
                tv_price = itemView.findViewById(R.id.price_tv);
                btn_increase = itemView.findViewById(R.id.increase_btn);
                btn_decrease = itemView.findViewById(R.id.decrease_btn);


                btn_increase.setOnClickListener(this);
                btn_decrease.setOnClickListener(this);
            }

            //make click function action here
            @Override
            public void onClick(View v) {

                ModelCart getCart  = dataCart.get(getAdapterPosition());

                if (v == btn_increase) {

                    //increase item data cart
                    int temp_jum = Integer.parseInt(tv_qty.getText().toString());
                    temp_jum += 1;
                    tv_qty.setText("" + temp_jum);
                    int total_price = temp_jum * getCart.item_price;
                    tv_price.setText("Rp. " + Separator.getInstance().doSeparate("" + total_price, Locale.GERMANY));
                    getCart.qty = temp_jum;
                    getCart.total_price = total_price;

                } else if (v == btn_decrease) {

                    //decrease item data cart
                    int temp_jum = Integer.parseInt(tv_qty.getText().toString());
                    //check decrease
                    if (temp_jum > 1) {
                        //if qty of item is more than one
                        //decrease quantity
                        temp_jum -= 1;

                        //=============== Update UI start =============//
                        //update text qty in item list
                        tv_qty.setText("" + temp_jum);
                        //update text total text in item list
                        int total_price = temp_jum * getCart.item_price;
                        //update text total with rupiah format
                        tv_price.setText("Rp. " + Separator.getInstance().doSeparate("" + total_price, Locale.GERMANY));
                        //=============== Update UI end =============//


                        //=============== Update Data start =============//
                        getCart.qty = temp_jum;
                        getCart.total_price = total_price;
                        //=============== Update Data end =============//

                    } else {
                        //if item is going to zero, than remove item

                        //=============== Update to Server start =============//
                        removeCart(getCart.id_cart);
                        //=============== Update to Server end =============//

                        //=============== Update UI start =============//
                        dataCart.remove(getCart);
                        adapterCart.notifyDataSetChanged();
                        //=============== Update UI end =============//
                    }

                }
            }
        }

    }


    private void removeCart(final int id_cart) {
        //showing the dialog
        customDialog.show();
        //setup request method(GET), URL
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.removeCart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //hide the dialog
                customDialog.dismiss();

                //renew the arraylist data
                dataCart = new ArrayList<>();

                //analyze JSON data structure from variable response -> .. onResponse(String response) {
                //try catch is must, if you parsing the json
                try {
                    //get json object of response
                    JSONObject parent = new JSONObject(response);
                    //get json array from parent json object
                    JSONArray jsonArray = parent.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        //get json object for i value position
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        //get the atribut depend of their name
                        int id_cart = jsonObject.getInt("id_cart");
                        int id_product = jsonObject.getInt("id_product");
                        int qty = jsonObject.getInt("qty");
                        int total_price = jsonObject.getInt("total_price");

                        String image = jsonObject.getString("image");
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        int item_price = jsonObject.getInt("item_price");

                        //create new row of data that representated in each model created
                        ModelCart modelCart = new ModelCart(
                                id_cart,
                                id_product,
                                qty,
                                total_price,
                                image,
                                name,
                                description,
                                item_price
                        );

                        //add to array list data
                        dataCart.add(modelCart);
                    }

                    //initialize and fill adapter product with data that have been returned from json
                    adapterCart = new AdapterCart(dataCart);

                    //connect recyclerview to adapter product
                    rv_cart.setAdapter(adapterCart);

                } catch (JSONException e) {
                    //when request parsing error, hide the dialog
                    customDialog.dismiss();
                    //print error message
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //when request to server error, hide the dialog
                customDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //send parameter to server that represent the id of item that will be deleted
                Map<String, String> param = new HashMap<String, String>();
                param.put("id_cart", "" + id_cart);
                return param;
            }
        };

        //applying to start the request data to server
        VolleyRequest.getInstance().addToRequestQueue(stringRequest);
    }

    private void updateCart() {
        //showing the dialog
        customDialog.show();
        //setup request method(GET), URL
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.updateCart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //hide the dialog
                customDialog.dismiss();

                //renew the arraylist data
                dataCart = new ArrayList<>();

                //analyze JSON data structure from variable response -> .. onResponse(String response) {
                //try catch is must, if you parsing the json
                try {
                    //get json object of response
                    JSONObject parent = new JSONObject(response);
                    //get json array from parent json object
                    JSONArray jsonArray = parent.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        //get json object for i value position
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        //get the attribute depend of their name
                        int id_cart = jsonObject.getInt("id_cart");
                        int id_product = jsonObject.getInt("id_product");
                        int qty = jsonObject.getInt("qty");
                        int total_price = jsonObject.getInt("total_price");
                        String image = jsonObject.getString("image");
                        String name = jsonObject.getString("name");
                        String description = jsonObject.getString("description");
                        int item_price = jsonObject.getInt("item_price");

                        //create new row of data that represented in each model created
                        ModelCart modelCart = new ModelCart(
                                id_cart,
                                id_product,
                                qty,
                                total_price,
                                image,
                                name,
                                description,
                                item_price
                        );

                        //add to array list data
                        dataCart.add(modelCart);
                    }

                    //initialize and fill adapter product with data that have been returned from json
                    adapterCart = new AdapterCart(dataCart);

                    //connect recyclerview to adapter cart
                    rv_cart.setAdapter(adapterCart);

                } catch (JSONException e) {
                    //when request parsing error, hide the dialog
                    customDialog.dismiss();
                    //print error message
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //when request to server error, hide the dialog
                customDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                //adding parameter for update data cart
                Map<String, String> param = new HashMap<String, String>();

                try {

                    //get data from data cart
                    ArrayList<ModelCart> dataCartParam = adapterCart.getData();

                    //create new json array
                    JSONArray jsonArray = new JSONArray();

                    //set data from cart list adapter to json object parameter that will be pass to the server
                    for (int i = 0; i < dataCartParam.size(); i++) {
                        JSONObject dataObj = new JSONObject();
                        dataObj.put("id_product", "" + dataCartParam.get(i).id_product);
                        dataObj.put("id_cart", "" + dataCartParam.get(i).id_cart);
                        dataObj.put("qty", "" + dataCartParam.get(i).qty);
                        dataObj.put("total_price", "" + dataCartParam.get(i).total_price);
                        jsonArray.put(dataObj);
                    }
                    //set data to one json array
                    param.put("data", jsonArray.toString());

                } catch (JSONException e) {
                    //print error message
                    e.printStackTrace();
                }

                return param;
            }
        };

        //applying to start the request data to server
        VolleyRequest.getInstance().addToRequestQueue(stringRequest);
    }

}