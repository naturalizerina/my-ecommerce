package com.kodingnext.ecommerce.activity;

import java.io.Serializable;

//implement serializable is need passing the data action
public class ModelProduct implements Serializable {

    int id;
    String name;
    String price;
    String description;
    String image;

    public ModelProduct(int id, String name, String price, String description, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.image = image;
    }
}
