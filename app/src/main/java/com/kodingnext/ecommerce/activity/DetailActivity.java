package com.kodingnext.ecommerce.activity;


import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kodingnext.ecommerce.AppConstant;
import com.kodingnext.ecommerce.CustomDialog;
import com.kodingnext.ecommerce.R;
import com.kodingnext.ecommerce.VolleyRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import naturalizer.separator.Separator;


public class DetailActivity extends AppCompatActivity {

    //initialize component
    ModelProduct dataProduct;
    AlertDialog alertDialog;
    //initialize quantity and total of add to cart item
    int qty = 0;
    int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //get serializable data from main activity
        dataProduct = (ModelProduct) getIntent().getSerializableExtra("data");

        //initialize component
        ImageView iv_product = findViewById(R.id.product_iv);
        TextView tv_name = findViewById(R.id.name_tv);
        TextView tv_price = findViewById(R.id.price_tv);
        TextView tv_desc = findViewById(R.id.desc_tv);

        //set data from -> dataProduct = (MainActivity.ModelProduct) getIntent().getSerializableExtra("data")
        Picasso.get().load(dataProduct.image).into(iv_product);
        tv_name.setText(dataProduct.name);
        tv_price.setText("Rp. " + Separator.getInstance().doSeparate(dataProduct.price, Locale.GERMANY));
        tv_desc.setText(dataProduct.description);
    }

    //adding click add to cart function
    public void addToCartDialog(View view) {

        //initialize dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_addtocart, viewGroup, false);
        builder.setView(dialogView);
        //user cannot hiding the dialog, directly except from code, like -> alertDialog.dismiss()
        builder.setCancelable(false);
        //create the dialog, that called as "alertDialog"
        alertDialog = builder.create();

        //initialize component of dialog
        TextView tv_description = dialogView.findViewById(R.id.desc_tv);
        final TextView tv_price = dialogView.findViewById(R.id.price_tv);
        ImageView iv_product = dialogView.findViewById(R.id.product_iv);
        final TextView tv_qty = dialogView.findViewById(R.id.qty_tv);
        Button btn_increase = dialogView.findViewById(R.id.increase_btn);
        Button btn_decrease = dialogView.findViewById(R.id.decrease_btn);
        Button btn_addtocart = dialogView.findViewById(R.id.addtocart_btn);
        ImageButton iv_close = dialogView.findViewById(R.id.close_iv);

        //set data of component dialog
        tv_description.setText(dataProduct.name);
        tv_price.setText("Rp. " + Separator.getInstance().doSeparate(dataProduct.price, Locale.GERMANY));
        Picasso.get().load(dataProduct.image).into(iv_product);
        //set default quantity
        tv_qty.setText("1");
        //set default total
        total = 1 * Integer.parseInt(dataProduct.price);

        //add function
        btn_addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doAddToCart();
            }
        });

        //increase function
        btn_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qty = Integer.parseInt(tv_qty.getText().toString());
                qty += 1;
                tv_qty.setText(String.valueOf(qty));
                total = qty * Integer.parseInt(dataProduct.price);
                tv_price.setText("Rp. " + Separator.getInstance().doSeparate(String.valueOf(total), Locale.GERMANY));
            }
        });

        //decrease function
        btn_decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (qty > 1) {
                    qty = Integer.parseInt(tv_qty.getText().toString());
                    qty -= 1;
                    tv_qty.setText(String.valueOf(qty));
                    total = qty * Integer.parseInt(dataProduct.price);
                    tv_price.setText("Rp. " + Separator.getInstance().doSeparate(String.valueOf(total), Locale.GERMANY));
                }
            }
        });

        //close function
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty = 0;
                total = 0;
                alertDialog.dismiss();
            }
        });

        //showing dialog
        alertDialog.show();
    }

    //adding the data to server
    private void doAddToCart() {

        //initialize the dialog
        final CustomDialog customDialog = new CustomDialog(DetailActivity.this);
        //showing the dialog
        customDialog.show();
        //setup request method(GET), URL
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstant.addToCart, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //hiding the dialog
                customDialog.dismiss();

                //analyze JSON data structure from variable response -> .. onResponse(String response) {
                //try catch is must, if you parsing the json
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //showing message from server
                    Toast.makeText(DetailActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    //hiding the alert dialog
                    alertDialog.dismiss();
                } catch (JSONException e) {
                    //hiding the alert dialog
                    alertDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //hiding dialog
                customDialog.dismiss();
                //hiding alertdialog
                alertDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //adding parameter that will be send to the server
                Map<String, String> param = new HashMap<String, String>();
                param.put("id_product", "" + dataProduct.id);
                //(Math.max(qty, 1)) -> the qty is max and the 1 is minimum
                param.put("qty", String.valueOf(Math.max(qty, 1)));
                param.put("total_price", String.valueOf(total));
                return param;
            }
        };

        //start request to the server
        VolleyRequest.getInstance().addToRequestQueue(stringRequest);
    }

}