package com.kodingnext.ecommerce;


import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

//this class is for creating custom request using volley
public class VolleyRequest extends Application {

    private static VolleyRequest mInstance;
    private RequestQueue mRequestQueue;

    public static synchronized VolleyRequest getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        mRequestQueue.add(req);
    }

}